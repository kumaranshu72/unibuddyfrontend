import React, { Component } from 'react';
import { map, find, findIndex } from 'lodash'

import './BlogList.css'

export default class BlogList extends Component {
  constructor(props) {
    super(props)
    const { data } = this.props
    const likesOb = map(data, (item, index)=>{
      const { id } = item
      return { id, likes: 0 }
    })
    this.state = {
      likes: likesOb,
    }
  }
  incrementLike = id => {
    const { likes } = this.state
    const index = findIndex(likes, { id })
    const tmpObj = likes;
    tmpObj[index].likes += 1;
    this.setState({
      likes: tmpObj,
    })
  }
  renderPostTile = () => {
    const { data } = this.props
    return map(data, (item, index) => {
      const { id, title, text, image, numberOfViews, readingTime, topic, author } = item
      const { likes } = this.state
      const postLike = find(likes, { id }).likes
      return (
        <div className='main' key={index}>
          <div className='userSection'>
            <div className='profileName'>
                <div className='profilePhoto'>
                  <img src={author.profilePhoto} className='pic'/>
                </div>
                <div className='titleContainer'>
                  <div className='firstName'>
                    {author.firstName}
                  </div>
                  <div className='degree'>
                    {author.degree.name}
                  </div>
                </div>
            </div>
            <div className='likeSection' onClick={() => this.incrementLike(id)}>
                <img src='https://image.flaticon.com/icons/svg/126/126473.svg' className='likeImg'/>
            </div>
          </div>
          <div className='imageSection'>
            <img src={image} width='100%'/>
          </div>
          <div className='blogSection'>
            <div>
              <h2 className='colorText'>{title}</h2>
            </div>
            <div>
              <p className='colorText'>
                {text}
              </p>
            </div>
            <div className='summarySection'>
              <div className='insideSection'>
                <p>{topic.name}</p>
                <p className='readTime'>{`${readingTime} min read`}</p>
              </div>
              <div className='insideSection'>
                <p>{`${numberOfViews} views`}</p>
                <p className='readTime'>{`${postLike} Likes`}</p>
              </div>
            </div>
          </div>
        </div>
      )
      return (
        <div>
          helo
        </div>
      )
    })
  }

  render() {
    return(
      <div>
        {this.renderPostTile()}
      </div>
    )
  }
} 